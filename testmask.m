function varargout = testmask(varargin)
% TESTMASK MATLAB code for testmask.fig
%      TESTMASK, by itself, creates a new TESTMASK or raises the existing
%      singleton*.
%
%      H = TESTMASK returns the handle to a new TESTMASK or the handle to
%      the existing singleton*.
%
%      TESTMASK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TESTMASK.M with the given input arguments.
%
%      TESTMASK('Property','Value',...) creates a new TESTMASK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before testmask_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to testmask_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help testmask

% Last Modified by GUIDE v2.5 07-Jul-2015 17:10:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @testmask_OpeningFcn, ...
                   'gui_OutputFcn',  @testmask_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before testmask is made visible.
function testmask_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to testmask (see VARARGIN)

% Choose default command line output for testmask
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes testmask wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = testmask_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in browse.
function browse_Callback(hObject, eventdata, handles)
% hObject    handle to browse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global listing index path image

index = 1;
path = uigetdir('E:\Data\Kuliah (!)\SEMESTER 8\Tugas Akhir 2\Pengujian ATV');
if path ~= 0
    path = [path '\'];
    listing = dir([path '*.jpg']);
    image = imread([path listing(index).name]);
    axes(handles.axes1);
    imshow(image);
    set(handles.photoname,'String',listing(index).name);
    set(handles.left, 'enable', 'on');
    set(handles.right, 'enable', 'on');
end


% --- Executes on button press in cutpaste.
function cutpaste_Callback(hObject, eventdata, handles)
% hObject    handle to cutpaste (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global image res
[panjang,lebar,dimensi] = size(image);
res = zeros([panjang lebar]);
rect = round(getrect);
res(rect(2):rect(2)+rect(4),rect(1):rect(1)+rect(3)) = 1;
axes(handles.axes2);
imshow(res);


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global res path index listing

if ~isempty(get(handles.defaultname,'String'))
    name = listing(index).name;
    underscore = find(name == '_');
    score = name(underscore(2)+1:end-4);
    imwrite(res,[path get(handles.defaultname,'String') '_' num2str(score) '.jpg']);
end


% --- Executes on button press in left.
function left_Callback(hObject, eventdata, handles)
% hObject    handle to left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global listing index path image
if index > 1
    index = index - 1;
    image = imread([path listing(index).name]);
    axes(handles.axes1);
    imshow(image);
    set(handles.photoname,'String',listing(index).name);
end


% --- Executes on button press in right.
function right_Callback(hObject, eventdata, handles)
% hObject    handle to right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global listing index path image
if index < size(listing,1)
    index = index + 1;
    image = imread([path listing(index).name]);
    axes(handles.axes1);
    imshow(image);
    set(handles.photoname,'String',listing(index).name);
end

function defaultname_Callback(hObject, eventdata, handles)
% hObject    handle to defaultname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of defaultname as text
%        str2double(get(hObject,'String')) returns contents of defaultname as a double


% --- Executes during object creation, after setting all properties.
function defaultname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to defaultname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
