function [ J ] = rotate( I, type )
%ROTATE Summary of this function goes here
%   Detailed explanation goes here
[baris kolom warnargb] = size(I);
temp=uint8(zeros(size(I)));
switch type
    case 'left'
        for i=1:baris
            k = baris;
            for j=1:kolom
                temp(k,i,:) = I(i,j,:);
                k=k-1;
            end
        end
    case 'right'
        k = kolom;
        for i=1:baris
            for j=1:kolom
                temp(j,k,:) = I(i,j,:);
            end
            k=k-1;
        end
end
J = temp;
end


