function level = threshold(I)
% Reference:
% N. Otsu, "A Threshold Selection Method from Gray-Level Histograms,"
% IEEE Transactions on Systems, Man, and Cybernetics, vol. 9, no. 1,
% pp. 62-66, 1979.

    % buat histogram
    num_bins = 256;
    counts = imhist(I,num_bins);

    % Otsu's method
    p = counts / sum(counts);
    omega = cumsum(p);
    mu = cumsum(p .* (1:num_bins)');
    mu_t = mu(end);
    sigma_b_squared = (mu_t * omega - mu).^2 ./ (omega .* (1 - omega));

    % cari index yang nilai sigma_b_squared terbesar
    maxval = max(sigma_b_squared);
    if isfinite(maxval)
        idx = mean(find(sigma_b_squared == maxval));
        % Normalisasi threshold
        level = (idx - 1) / (num_bins - 1);
    else
        level = 0.0;
    end
end

