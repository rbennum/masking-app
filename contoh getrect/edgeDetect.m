function [ J ] = edgeDetect( I, type )
%EDGEDETECT Summary of this function goes here
%   Detailed explanation goes here
    if size(I,3)==3
        I = rgb2gray(I);
    end
    switch type
        case 'robert'
            gx = [-1 0 0; 0 1 0; 0 0 0];
            gy = [0 0 -1; 0 1 0; 0 0 0];

        case 'prewitt1'
            gx = [-1 0 1; -1 0 1; -1 0 1];
            gy = [-1 -1 -1; 0 0 0; 1 1 1];

        case 'sobel'
            gx = [-1 0 1; -2 0 2; -1 0 1];
            gy = [-1 -2 -1; 0 0 0; 1 2 1];

        case 'freichan'
            gx = [-1 0 1; -sqrt(2) 0 sqrt(2); -1 0 1];
            gy = [-1 -sqrt(2) -1; 0 0 0; 1 sqrt(2) 1];
    end
    GX = konvolusi(I,gx);
    GY = konvolusi(I,gy);
    J = gabung(GX,GY);
end

