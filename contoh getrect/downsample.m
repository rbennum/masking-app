function [ J ] = downsample(I,tingkat)
%KUANTISASI Summary of this function goes here
%   Detailed explanation goes here
I = double(I);
J=I;
[m,n,o]=size(I); 
for k=1:o 
    for i=1:m/tingkat
         for j=1:n/tingkat
             jumlah = sum(sum(I((i-1)*tingkat+1:i*tingkat,(j-1)*tingkat+1:j*tingkat,k)));
             mean = jumlah/(tingkat*tingkat);
             J((i-1)*tingkat+1:i*tingkat,(j-1)*tingkat+1:j*tingkat,k)= mean;
         end 
    end 
end 
J=uint8(J);
end

