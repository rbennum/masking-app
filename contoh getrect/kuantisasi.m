function [ J ] = kuantisasi(I,tingkat)
%KUANTISASI Summary of this function goes here
%   Detailed explanation goes here
J=I;
I = double(I);
[m,n,o]=size(I); 
for k=1:o 
    for i=1:m 
         for j=1:n 
             J(i,j,k)=bitshift(I(i,j,k),tingkat);
         end 
    end 
end 
J=uint8(J);
end

