function [ J ] = filtering( I, type, tingkat )
%FILTERING Summary of this function goes here
%   Detailed explanation goes here
    
    switch type
        case 'mean'
            mask(1:tingkat,1:tingkat) = 1/(tingkat*tingkat);
            J = konvolusi(I,mask);
        case 'median'
            J = medianFiltering(I,tingkat);
        case 'modus'
            J = modusFiltering(I,tingkat);
    end
end

