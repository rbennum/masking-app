function hx = modusFiltering(fx,tingkat)
    fx = double(fx);
    gx = zeros(tingkat,tingkat);
    
    [rowFx colFx rgbFx] = size(fx);
    [rowGx colGx] = size(gx);

    % padding Fx dengan angka 0
    padval = floor(rowGx/2);
    fxpad = zeros(rowFx+(2*padval),colFx+(2*padval),rgbFx);
    for h=1:rgbFx    
        for i=1:rowFx
            for j=1:colFx
                fxpad(i+padval,j+padval,h) = fx(i,j,h);
            end
        end
    end

    %perhitungan dimulai
    hx = zeros(rowFx,colFx);
    for h=1:rgbFx
        for i=1:rowFx
            for j=1:colFx
                gx = fxpad(i:i+(2*padval),j:j+(2*padval),h);
                gx = reshape(gx,1,tingkat*tingkat);
                hx(i,j,h) = mode(gx);
            end
        end
    end
    hx = uint8(hx);
end