function [ J ] = flip( I, type )
%ROTATE Summary of this function goes here
%   Detailed explanation goes here
[baris kolom warnargb] = size(I);
temp=uint8(zeros(size(I)));
switch type
    case 'horizontal'
        for i=1:baris
            k = baris;
            for j=1:kolom
                temp(i,k,:) = I(i,j,:);
                k=k-1;
            end
        end
    case 'vertikal'
        k = kolom;
        for i=1:baris
            for j=1:kolom
                temp(k,j,:) = I(i,j,:);
            end
            k=k-1;
        end
end
J = temp;
end


