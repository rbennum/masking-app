function varargout = GUI(varargin)
% GUI M-file for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 21-Dec-2014 14:31:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pilihgambar.
function pilihgambar_Callback(hObject, eventdata, handles)
% hObject    handle to pilihgambar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%mengambil path gambar
[filename pathname] = uigetfile( {'*.bmp;','BMP Files(*.bmp)'; '*.*',  'All Files (*.*)'}, 'Pilih gambar');
fullpath = strcat(pathname,filename);

global I;
global J;
I = imread(fullpath);
J=I;
axes(handles.axesMain);
imshow(I);


% --- Executes on button press in reset.
function reset_Callback(hObject, eventdata, handles)
% hObject    handle to reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
global I;
J=I;
axes(handles.axesMain);
imshow(J);

% --- Executes on slider movement.
function kuantisasi_Callback(hObject, eventdata, handles)
% hObject    handle to kuantisasi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of
%        slider
global J;
global I;
tingkat=round(get(hObject,'Value'))-5
setTingkat = round(get(hObject,'Value'));
set(hObject,'Value', setTingkat);
J=kuantisasi(J,tingkat);
imshow(J);

% --- Executes during object creation, after setting all properties.
function kuantisasi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to kuantisasi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function downsample_Callback(hObject, eventdata, handles)
% hObject    handle to downsample (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global J;
global I;
tingkat=2.^round(get(hObject,'Value'))
setTingkat = get(hObject,'Value');
set(hObject,'Value', setTingkat);
J = downsample(J,tingkat);
imshow(J);

% --- Executes during object creation, after setting all properties.
function downsample_CreateFcn(hObject, eventdata, handles)
% hObject    handle to downsample (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on button press in rotateLeft.
function rotateLeft_Callback(hObject, eventdata, handles)
% hObject    handle to rotateLeft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J = rotate(J,'left');
imshow(J);

% --- Executes on button press in rotateRight.
function rotateRight_Callback(hObject, eventdata, handles)
% hObject    handle to rotateRight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J = rotate(J,'right');
imshow(J);


% --- Executes on button press in flipHorizontal.
function flipHorizontal_Callback(hObject, eventdata, handles)
% hObject    handle to flipHorizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J = flip(J,'horizontal');
imshow(J);

% --- Executes on button press in flipVertikal.
function flipVertikal_Callback(hObject, eventdata, handles)
% hObject    handle to flipVertikal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J = flip(J,'vertikal');
imshow(J);


% --- Executes on button press in zoomOut.
function zoomOut_Callback(hObject, eventdata, handles)
% hObject    handle to zoomOut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J = imzoom(J,'out',2);
figure,imshow(J);

% --- Executes on button press in zoomIn.
function zoomIn_Callback(hObject, eventdata, handles)
% hObject    handle to zoomIn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J = imzoom(J,'in',2);
figure,imshow(J);


% --- Executes on button press in ekualisasi.
function ekualisasi_Callback(hObject, eventdata, handles)
% hObject    handle to ekualisasi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
K = ekualisasi(J);
axes(handles.axesMain);
imshow(K);

figure
subplot(2,2,1);
imshow(J);
subplot(2,2,2);
imhist(J(:,:,1));
subplot(2,2,3);
imshow(K);
subplot(2,2,4);
imhist(K(:,:,1));

J=K;


% --- Executes on button press in medianFiltering.
function medianFiltering_Callback(hObject, eventdata, handles)
% hObject    handle to medianFiltering (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
global tingkat;
J = filtering(J,'median',tingkat);
imshow(J);

% --- Executes on button press in meanFiltering.
function meanFiltering_Callback(hObject, eventdata, handles)
% hObject    handle to meanFiltering (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
global tingkat;
J = filtering(J,'mean',tingkat);
imshow(J);

% --- Executes on button press in modusFiltering.
function modusFiltering_Callback(hObject, eventdata, handles)
% hObject    handle to modusFiltering (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
global tingkat;
J = filtering(J,'modus',tingkat);
imshow(J);

% --- Executes on slider movement.
function filteringSlider_Callback(hObject, eventdata, handles)
% hObject    handle to filteringSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global tingkat;
tingkat = round(get(hObject,'Value'))*2+1;
setTingkat = round(get(hObject,'Value'));
set(hObject,'Value', setTingkat);
set(handles.filteringTextField,'String',int2str(tingkat));

% --- Executes during object creation, after setting all properties.
function filteringSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filteringSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function filteringTextField_Callback(hObject, eventdata, handles)
% hObject    handle to filteringTextField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of filteringTextField as text
%        str2double(get(hObject,'String')) returns contents of filteringTextField as a double


% --- Executes during object creation, after setting all properties.
function filteringTextField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filteringTextField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in prewitt1.
function prewitt1_Callback(hObject, eventdata, handles)
% hObject    handle to prewitt1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J = edgeDetect(J,'prewitt1');
imshow(J);

% --- Executes on button press in robertGradient.
function robertGradient_Callback(hObject, eventdata, handles)
% hObject    handle to robertGradient (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J = edgeDetect(J,'robert');
imshow(J);

% --- Executes on button press in sobel.
function sobel_Callback(hObject, eventdata, handles)
% hObject    handle to sobel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J = edgeDetect(J,'sobel');
imshow(J);

% --- Executes on button press in freichan.
function freichan_Callback(hObject, eventdata, handles)
% hObject    handle to freichan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
J = edgeDetect(J,'freichan');
imshow(J);


% --- Executes on button press in laplace.
function laplace_Callback(hObject, eventdata, handles)
% hObject    handle to laplace (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
if size(J,3)==3
    J = rgb2gray(J);
end
% gx = [0 1 0; 1 -4 1; 0 1 0];
gx = [-1 -1 -1; -1 8 -1; -1 -1 -1];
J = konvolusi(J,gx);
imshow(J);


% --- Executes on button press in prewitt2.
function prewitt2_Callback(hObject, eventdata, handles)
% hObject    handle to prewitt2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
if size(J,3)==3
    J = rgb2gray(J);
end
g1 = [-1 -1 -1; 1 -2 1; 1 1 1];
g2 = [1 -1 -1; 1 -2 -1; 1 1 1];
g3 = [1 1 -1; 1 -2 -1; 1 1 -1];
g4 = [1 1 1; 1 -2 -1; 1 -1 -1];
g5 = [1 1 1; 1 -2 1; -1 -1 -1];
g6 = [1 1 1; -1 -2 1; -1 -1 1];
g7 = [-1 1 1; -1 -2 1; -1 1 1];
g8 = [-1 -1 1; -1 -2 1; 1 1 1];
 
G1 = double(konvolusi(J,g1));
G2 = double(konvolusi(J,g2));
G3 = double(konvolusi(J,g3));
G4 = double(konvolusi(J,g4));
G5 = double(konvolusi(J,g5));
G6 = double(konvolusi(J,g6));
G7 = double(konvolusi(J,g7));
G8 = double(konvolusi(J,g8));
 
[row col] = size(G1);
G = zeros(row,col);
for i=1:row
	for j=1:col
    	G(i,j) = sqrt(G1(i,j).^2+G2(i,j).^2+G3(i,j).^2+G4(i,j).^2+G5(i,j).^2+G6(i,j).^2+G7(i,j).^2+G8(i,j).^2);
	end
end
 
J = uint8(G);
imshow(J);


% --- Executes on button press in threshold.
function threshold_Callback(hObject, eventdata, handles)
% hObject    handle to threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
if size(J,3)==3
    J = rgb2gray(J);
end
thresh = ones(size(J))*str2num(get(handles.threshField,'String'));
J = J>=thresh;
J = uint8(J);
J = J*255;
imshow(J);


% --- Executes on button press in erosi.
function erosi_Callback(hObject, eventdata, handles)
% hObject    handle to erosi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
SE = strel('diamond', 2);
J = imerode(J,SE);
imshow(J);

% --- Executes on button press in dilasi.
function dilasi_Callback(hObject, eventdata, handles)
% hObject    handle to dilasi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
SE = strel('diamond', 2);
J = imdilate(J,SE);
imshow(J);

% --- Executes on button press in cutnpaste.
function cutnpaste_Callback(hObject, eventdata, handles)
% hObject    handle to cutnpaste (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
rect = round(getrect);
Cut = J(rect(2):rect(2)+rect(4),rect(1):rect(1)+rect(3),:);
J(rect(2):rect(2)+rect(4),rect(1):rect(1)+rect(3),:) = 0;
imshow(J);
figure,imshow(Cut);



function threshField_Callback(hObject, eventdata, handles)
% hObject    handle to threshField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of threshField as text
%        str2double(get(hObject,'String')) returns contents of threshField as a double


% --- Executes during object creation, after setting all properties.
function threshField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to threshField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
