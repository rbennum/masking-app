function hx = konvolusi(fx,gx)
    fx = double(fx);
    gx = double(gx);
    
    [rowFx colFx rgbFx] = size(fx);
    [rowGx colGx] = size(gx);

    % padding Fx dengan angka 0
    padval = floor(rowGx/2);
    fxpad = zeros(rowFx+(2*padval),colFx+(2*padval),rgbFx);
    for h=1:rgbFx    
        for i=1:rowFx
            for j=1:colFx
                fxpad(i+padval,j+padval,h) = fx(i,j,h);
            end
        end
    end

    %perhitungan dimulai
    hx = zeros(rowFx,colFx);
    for h=1:rgbFx
        for i=1:rowFx
            for j=1:colFx
                hasil = sum(sum(fxpad(i:i+(2*padval),j:j+(2*padval),h).*gx));
                if hasil<0
                    hx(i,j,h) = 0;
                elseif hasil>255
                    hx(i,j,h) = 255;
                else
                    hx(i,j,h) = hasil;
                end
            end
        end
    end
    hx = uint8(hx);

end