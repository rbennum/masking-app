function hasil = gabung( GX,GY )
%GABUNG Summary of this function goes here
%   Detailed explanation goes here

    GX = double(GX);
    GY = double(GY);
    [row col] = size(GX);
    hasil = zeros(row,col);
    for i=1:row
        for j=1:col
            hasil(i,j) = sqrt(GX(i,j).^2+GY(i,j).^2);
        end
    end
    
    hasil = uint8(hasil);
end

